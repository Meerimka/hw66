import React, {Component, Fragment} from 'react';
import Modal from "../components/UI/Modal/Modal";
import Spinner from "../components/UI/Spinner/Spinner";


const WithLoaderHandle = (WrappedComponents, axios)=> {
    return class WithLoaderHOC extends Component {
        constructor(props){
            super(props);
            this.state={
                error:null,
                loader: false,
            } ;

            this.state.interceptorIdres = axios.interceptors.response.use(res => {
                this.setState({loader:false});
                return res;
            } ,error =>{
                this.setState({error});
                throw error;

            });
            this.state.interseptorsIdreg = axios.interceptors.request.use(reg =>{
                this.setState({loader:true});
                return reg;
            })
        };

        errordismissed = () =>{
            this.setState({error: null})
        };



        componentWillUnmount(){
            axios.interceptors.request.eject(this.state.interseptorsIdreg);
            axios.interceptors.response.eject(this.state.interceptorIdres);
        }

        render(){
            return (
                <Fragment>
                    <Modal show={this.state.error} close={this.errordismissed}>
                        {this.state.error && this.state.error.message}
                    </Modal>
                    {this.state.loader ? <Spinner/> : null}
                    <WrappedComponents {...this.props}/>
                </Fragment>
            )
        }
    }
};

export default WithLoaderHandle;