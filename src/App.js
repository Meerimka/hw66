import React, { Component } from 'react';
import { Route, Switch, BrowserRouter } from 'react-router-dom';
import './App.css';
import Home from './containers/Home/Home.js';
import Add from './components/Services/Add.js';
import AboutUs from './components/Services/AboutUs.js';
import Contacts from './components/Services/Contacts.js';
import Post from "./components/Services/Post";
import Edit from "./components/Services/Edit";

class App extends Component {
  render() {
    return (
        <BrowserRouter>
          <Switch>
            <Route path="/" exact component={Home}></Route>
            <Route path="/add" component={Add}></Route>
            <Route path="/aboutus" component={AboutUs}/>
            <Route path="/contact" component={Contacts}></Route>
            <Route path="/posts/:id/edit" component={Edit}></Route>
            <Route path="/posts/:id" component={Post}></Route>
          </Switch>
        </BrowserRouter>
    );
  }
}

export default App;
