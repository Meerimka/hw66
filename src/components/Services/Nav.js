import React, {Component} from 'react';
import {NavLink} from "react-router-dom";

class Nav extends Component {
    render() {
        return (
            <div className='nav'>
                <NavLink to="/">Home</NavLink>
                <NavLink to='/add'>Add</NavLink>
                <NavLink to="/aboutus">About Us</NavLink>
                <NavLink to="/contact">Contacts</NavLink>
            </div>
        );
    }
}

export default Nav;