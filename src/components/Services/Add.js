import React, {Component} from 'react';
import Nav from "./Nav";
import axios from '../../axios-posts';

class Add extends Component {
    state = {
        title :'',
        text :'',
        id: '',
    };
    valueChanged =(event) =>{
        const name = event.target.name;
        this.setState({[name]:event.target.value})
    };


    publishHandler=event=>{
        event.preventDefault();

        const publish ={
            title: this.state.title,
            text:this.state.text
        };

        axios.post( '/posts.json',publish).then(() =>{
            this.props.history.push('/');
        });

    };
    render() {
        return (
            <div className='post'>
                <Nav></Nav>
                <h2>Add new post</h2>
                <div className='post-box'>
                    <input className='post-title' value={this.state.title} onChange={this.valueChanged}
                           type="text" placeholder="Type post title here" name="title"/>
                    <textarea id="postbody" value={this.state.text} onChange={this.valueChanged}
                           cols="30" rows="10" name="text"/>
                    <button className='post-btn' onClick={this.publishHandler}>Save</button>
                </div>
            </div>
    )
        ;
    }
}

export default Add;