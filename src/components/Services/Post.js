import React, {Component, Fragment,} from 'react';
import axios from '../../axios-posts';
import {NavLink} from "react-router-dom";

class Post extends Component {

    state = {
        post: {
            title: '',
            text:'',
        }
    };

    componentDidMount() {
        const id=this.props.match.params.id;

        axios.get(`/posts/${id}.json`).then(response=>{
            this.setState({post: response.data})
        })
    }
    remove=()=> {
        const id = this.props.match.params.id;
        axios.delete(`/posts/${id}.json`).then(() => {
            this.props.history.push('/');
        });
    };



    render() {
        return (
            <Fragment>
                <h2>{this.state.post.title}</h2>
                <p>{this.state.post.text}</p>
                <button onClick={()=> this.remove()}>X</button>
                <NavLink to={'/posts/' + this.props.match.params.id + '/edit'}><button>Edit</button></NavLink>
            </Fragment>
        );
    }
}

export default Post;