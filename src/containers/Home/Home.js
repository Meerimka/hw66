import React, {Component, Fragment} from 'react';
import Nav from "../../components/Services/Nav";
import axios from '../../axios-posts';
import WithLoaderHandle from "../../hoc/withLoader";
import Spinner from "../../components/UI/Spinner/Spinner";

class Home extends Component {
    state = {
        posts: [],
        loading: false,
    };


    componentDidMount(){
        axios.get('/posts.json').then(response =>{
            let posts = [];
            for(let key in response.data) {

                posts.push({title: response.data[key].title , text: response.data[key].text, id: key})
            }
            this.setState({posts})
        })
    };

    readMore=(id) => {
        this.props.history.push(`/posts/${id}`)
    };

    render() {
        if(this.state.loading){
            return <Spinner/>
        }
        return (
            <Fragment>
                <Nav/>
                {this.state.posts.map((post,index)=>{
                    return(
                        <div key={index}>
                            <h2>{post.title}</h2>
                            <p>
                                {post.text}
                            </p>
                            <button onClick={()=>{this.readMore(post.id)}}>read more</button>
                        </div>
                    )
                })}
            </Fragment>
        )
    }
}

export default WithLoaderHandle( Home ,axios);